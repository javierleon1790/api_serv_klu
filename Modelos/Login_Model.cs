﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Serv_Klu.Modelos
{
    public class Login_Model
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
