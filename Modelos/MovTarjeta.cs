﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Serv_Klu.Modelos
{
    public class MovTarjeta
    {
        public int Id_Tarjeta { get; set; }
        public decimal Monto { get; set; }
        public string Fecha_Mov { get; set; }
        
    }
}
