﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Api_Serv_Klu.Modelos.BDKlu
{
    public partial class UsuarioKlu
    {
        public UsuarioKlu()
        {
            TarjetaKlus = new HashSet<TarjetaKlu>();
        }

        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public byte Edad { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<TarjetaKlu> TarjetaKlus { get; set; }
    }
}
