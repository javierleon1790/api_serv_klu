﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Api_Serv_Klu.Modelos.BDKlu
{
    public partial class KluDbContext : DbContext
    {
        public KluDbContext()
        {
        }

        public KluDbContext(DbContextOptions<KluDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MovimientoTarjetum> MovimientoTarjeta { get; set; }
        public virtual DbSet<TarjetaKlu> TarjetaKlus { get; set; }
        public virtual DbSet<UsuarioKlu> UsuarioKlus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=Localhost;Database=KluDb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<MovimientoTarjetum>(entity =>
            {
                entity.HasKey(e => e.IdMovimiento);

                entity.ToTable("Movimiento_Tarjeta");

                entity.Property(e => e.IdMovimiento).HasColumnName("Id_Movimiento");

                entity.Property(e => e.FechaMov)
                    .HasColumnType("datetime")
                    .HasColumnName("Fecha_Mov");

                entity.Property(e => e.IdTarjeta).HasColumnName("Id_Tarjeta");

                entity.Property(e => e.Monto).HasColumnType("money");

                entity.HasOne(d => d.IdTarjetaNavigation)
                    .WithMany(p => p.MovimientoTarjeta)
                    .HasForeignKey(d => d.IdTarjeta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimiento_Tarjeta_Tarjeta_Klu");
            });

            modelBuilder.Entity<TarjetaKlu>(entity =>
            {
                entity.HasKey(e => e.IdTarjeta);

                entity.ToTable("Tarjeta_Klu");

                entity.Property(e => e.IdTarjeta).HasColumnName("Id_Tarjeta");

                entity.Property(e => e.FechaExp)
                    .HasColumnType("date")
                    .HasColumnName("Fecha_Exp");

                entity.Property(e => e.IdUsuario).HasColumnName("Id_Usuario");

                entity.Property(e => e.NumeroTarjeta)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("Numero_Tarjeta");

                entity.Property(e => e.Saldo).HasColumnType("money");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.TarjetaKlus)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tarjeta_Klu_Usuario_Klu");
            });

            modelBuilder.Entity<UsuarioKlu>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("Usuario_Klu");

                entity.Property(e => e.IdUsuario).HasColumnName("Id_Usuario");

                entity.Property(e => e.ApellidoM)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("Apellido_M");

                entity.Property(e => e.ApellidoP)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("Apellido_P");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
