﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Api_Serv_Klu.Modelos.BDKlu
{
    public partial class TarjetaKlu
    {
        public TarjetaKlu()
        {
            MovimientoTarjeta = new HashSet<MovimientoTarjetum>();
        }

        public int IdTarjeta { get; set; }
        public int IdUsuario { get; set; }
        public string NumeroTarjeta { get; set; }
        public bool Credito { get; set; }
        public DateTime FechaExp { get; set; }
        public decimal Saldo { get; set; }

        public virtual UsuarioKlu IdUsuarioNavigation { get; set; }
        public virtual ICollection<MovimientoTarjetum> MovimientoTarjeta { get; set; }
    }
}
