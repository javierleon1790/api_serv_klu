﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Api_Serv_Klu.Modelos.BDKlu
{
    public partial class MovimientoTarjetum
    {
        public int IdMovimiento { get; set; }
        public int IdTarjeta { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaMov { get; set; }

        public virtual TarjetaKlu IdTarjetaNavigation { get; set; }
    }
}
