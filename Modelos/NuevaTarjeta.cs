﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Serv_Klu.Modelos
{
    public class NuevaTarjeta
    {
        public int Id_Usuario { get; set; }
        public string Numero_Tarjeta { get; set; }
        public bool Credito { get; set; }
        public DateTime Fecha_Exp  { get; set; }
        public decimal Saldo { get; set; }
    }
}
