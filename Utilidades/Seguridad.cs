﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Api_Serv_Klu.Modelos;

namespace Api_Serv_Klu.Utilidades
{
    public class Seguridad
    {
        private readonly IConfiguration _configuration;

        public Seguridad(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        /// <summary>
        /// Funcion para generar token de usuario
        /// </summary>
        /// <param name="User">Modelo de datos para crear el token</param>
        /// <returns></returns>
        public string NuevoToken(Usuario User)
        {
            var Claims = new List<Claim>()
            {
             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
             new Claim(ClaimTypes.Name,User.Nombre),
              new Claim("Email", User.Email ),
                new Claim( "IdCliente", User.IdUsuario+"" )

            };

            //Header:
            var llave = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("KeyJwt")));
            var Cred = new SigningCredentials(llave, SecurityAlgorithms.HmacSha256);
            var _Header = new JwtHeader(Cred);

            //Payload
            var _Payload = new JwtPayload(
                   issuer: _configuration.GetValue<string>("Issuer"),
                   audience: _configuration.GetValue<string>("Audience"),
                   claims: Claims,
                   notBefore: DateTime.UtcNow,
                   expires: DateTime.UtcNow.AddMonths(12)
               );
            // GENERAMOS EL TOKEN //      
            var _token = new JwtSecurityToken(_Header, _Payload);
            return new JwtSecurityTokenHandler().WriteToken(_token);
        }
    
    }
}
