﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Api_Serv_Klu.Modelos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace Api_Serv_Klu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarjetasController : ControllerBase
    {
        // Post: api/AñadirTarjeta
        [HttpPost]
        [Route("/AñadirTarjeta")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> AñadirTarjeta([FromBody] NuevaTarjeta Nueva_Tarjeta)
        {
            using (Modelos.BDKlu.KluDbContext bd = new Modelos.BDKlu.KluDbContext())
            {
                var Tarjeta = (from dt in bd.TarjetaKlus
                               where dt.NumeroTarjeta == Nueva_Tarjeta.Numero_Tarjeta
                               select dt.NumeroTarjeta).FirstOrDefault();
                if (Tarjeta != null)
                {
                    return BadRequest("La tarjeta ya existe");// ;

                }
                else
                {
                    bd.TarjetaKlus.Add(
                         new Modelos.BDKlu.TarjetaKlu
                         {
                             NumeroTarjeta = Nueva_Tarjeta.Numero_Tarjeta,
                             IdUsuario = Nueva_Tarjeta.Id_Usuario,
                             Credito = Nueva_Tarjeta.Credito,
                             FechaExp = Nueva_Tarjeta.Fecha_Exp,
                             Saldo = Nueva_Tarjeta.Saldo
                         });
                    bd.SaveChanges();
                    return Ok("Exitoso");
                }
            }
        }

        [HttpPost]
        [Route("/Movimiento")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Movimiento([FromBody] MovTarjeta Mov_Tarjeta)
        {
            using (Modelos.BDKlu.KluDbContext bd = new Modelos.BDKlu.KluDbContext())
            {
                var Tarjeta = (from dt in bd.TarjetaKlus
                               where dt.IdTarjeta == Mov_Tarjeta.Id_Tarjeta
                               select dt).FirstOrDefault();
                if (Tarjeta != null)
                {
                    if (Tarjeta.Saldo >= Mov_Tarjeta.Monto)
                    {
                        var Nuevosaldo = bd.TarjetaKlus.Where(x => x.IdTarjeta == Tarjeta.IdTarjeta).FirstOrDefault();
                        Nuevosaldo.Saldo = Nuevosaldo.Saldo - Mov_Tarjeta.Monto;
                        bd.MovimientoTarjeta.Add(
                        new Modelos.BDKlu.MovimientoTarjetum
                        {
                            IdTarjeta = Mov_Tarjeta.Id_Tarjeta,
                            Monto = Mov_Tarjeta.Monto,
                            FechaMov = DateTime.Now
                        });
                        bd.SaveChanges();
                        return Ok("Transaccion realizada con exito");
                    }
                    else
                    {
                        return Ok("Saldo insuficiente");
                    }
                }
                else
                {
                    return BadRequest("La tarjeta no existe");
                }
            }
        }
    }
}
