﻿using Api_Serv_Klu.Modelos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Serv_Klu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccesoController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public AccesoController(IConfiguration configuration) 
        {
            _configuration = configuration;
        }

        //Post: api/Login        
        [Route("/Login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] Login_Model login_)
        {
            using (Modelos.BDKlu.KluDbContext bd = new Modelos.BDKlu.KluDbContext())
            {
                var Usuario = (from dt in bd.UsuarioKlus
                               where dt.Email == login_.Email && dt.Password == login_.Password
                               select dt.Nombre).FirstOrDefault();
                if (Usuario != null)
                {
                    var User = bd.UsuarioKlus.Where(x => x.Email == login_.Email).FirstOrDefault();
                    Usuario Userlog = new Usuario()
                    {
                        IdUsuario = User.IdUsuario,
                        Nombre = User.Nombre,
                        Email = User.Email
                    };
                    var token = new Utilidades.Seguridad(_configuration).NuevoToken(Userlog);
                    return Ok(token);// ;

                }
                else
                {
                    return BadRequest("El usuario no existe");
                }
            }
        }

        // Post: api/NuevoUsuario
        [AllowAnonymous]
        [Route("/NuevoUsuario")]
        [HttpPost]
        public async Task<ActionResult> NuevoUsuario([FromBody] Usuario User)
        {
            using (Modelos.BDKlu.KluDbContext bd = new Modelos.BDKlu.KluDbContext())
            {
                var Usuario = (from dt in bd.UsuarioKlus
                               where dt.Email == User.Email
                               select dt.Nombre).FirstOrDefault();
                if (Usuario != null)
                {
                    return Ok("El usuario ya existe: " +Usuario);
                }
                else
                {
                    
                        bd.UsuarioKlus.Add(
                            new Modelos.BDKlu.UsuarioKlu
                            {
                                Nombre = User.Nombre,
                                ApellidoP = User.ApellidoP,
                                ApellidoM = User.ApellidoM,
                                Edad = User.Edad,
                                Email = User.Email,
                                Password=User.Password
                            });
                        bd.SaveChanges();
                        User.IdUsuario = bd.UsuarioKlus.Where(x => x.Email == User.Email).FirstOrDefault().IdUsuario;
                    
                    return Ok(new Utilidades.Seguridad(_configuration).NuevoToken(User));
                }
            }
        }


    }
}
